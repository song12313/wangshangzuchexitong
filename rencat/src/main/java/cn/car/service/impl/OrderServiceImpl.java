package cn.car.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.car.dao.OrderDao;
import cn.car.entity.Order;
import cn.car.service.OrderService;

@Service("orderService")
public class OrderServiceImpl extends BaseServiceImpl<Order> implements
		OrderService {

	private OrderDao orderDao;
	@Resource
	public void setOrderDao(OrderDao orderDao) {
		super.setBaseDao(orderDao);
		this.orderDao = orderDao;
	}
	
	public List<Order> findObjectByUser(String userid) {
		return orderDao.findObjectByUserId(userid);
	}

	public List<Order> findObjectByStateOrUser(String state,String uid) {
		if(uid!=null && !"".equals(uid)){
			return orderDao.findObjectByStateAndUser(state,uid);
			
		}else{
			return orderDao.findObjectByState(state);
		}
	}

	public void changeOrderState(String sid, String state) {
		orderDao.changeOrderState(sid,state);
	}
}

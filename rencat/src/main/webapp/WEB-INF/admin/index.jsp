<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>车辆后台</title>
  <%@include file="common.jspf" %>
  <link rel="stylesheet" href="${basePath }acss/index.css" />
</head>
<body>
	<div class="_top">
		<div class="_logo">
			<p>自由族</p>
		</div>
		<div class="_user">
			<p><a href="javascript:alert('只有超级管理员才有增加,修改车辆的权利')">帮助</a></p>
			<p><a href="quit.action">退出</a></p>
			<p>${ADMININFO.name }</p>
			<img class="_user_img" src="${basePath }aimg/adminImg.png"/>
		</div>
	</div>
	<div class="_container">
		<div id="_left" class="_left">
			<ul class="_menu">
				<li>
					<a class="_menuItem" href="#"><span class="_circleIcon"></span>数据分析</a>
					<ul class="_menuItemMenu">
						<li><a class="_menuItemMenuItem" target="main" href="newUser.action" ><span class="_circleIconSmall"></span>新建用户统计</a></li>
						<li ><a class="_menuItemMenuItem" target="main" href="carCansus.action" ><span class="_circleIconSmall"></span>车辆租赁统计</a></li>
					</ul>
				</li>
				<li>
					<a class="_menuItem" href="#"><span class="_circleIcon"></span>用户信息管理</a>
					<ul class="_menuItemMenu">
						<li><a class="_menuItemMenuItem" target="main" href="userList.action" ><span class="_circleIconSmall"></span>用户资料</a></li>
					</ul>
				</li>
				<li>
					<a class="_menuItem" href="#"><span class="_circleIcon"></span>车辆信息管理</a>
					<ul class="_menuItemMenu">
						<li ><a class="_menuItemMenuItem" target="main" href="carList.action"><span class="_circleIconSmall"></span>车辆列表</a></li>
					</ul>
				</li>
				<li>
					<a class="_menuItem" href="#"><span class="_circleIcon"></span>订单管理</a>
					<ul class="_menuItemMenu">
						<li ><a class="_menuItemMenuItem" target="main" href="orderListUI.action" ><span class="_circleIconSmall"></span>订单列表</a></li>
						<li ><a class="_menuItemMenuItem" href="#"><span class="_circleIconSmall"></span>历史订单</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div id="_main" class="_main">
			<iframe id="main" name="main" scrolling="no" frameborder="0" src="newUser.action"></iframe>
		</div>
	</div>
	<script language="JavaScript">
		(function(){
				//拿到一级菜单项
				var lis = $('._menu > li');
				//添加鼠标单击添加样式事件,改变一级和二级菜单项样式
				$.each(lis, function(i,v) {
					//一级菜单项
					v.onclick = function(){
						$(v).toggleClass('active');
					}
					//对应的子菜单项
					var lilis = $(v).find("li");
					for(var i=0;i<lilis.length;i++){
						lilis[i].onclick =  function(){
							$(v).toggleClass('active');
							
							//去除之前选中元素的样式
							var _as = $('._active');
							_as.removeClass("_active");
							//为自身添加选择样式
							$(this).addClass("_active");
						}
					}
				});
			})();
			
			function changeDivSize(){
			   	var ifm= document.getElementById("main"); 
			    ifm.width= document.documentElement.clientWidth-230;
			    var left = document.getElementById("_left");
			    left.style.height = document.documentElement.clientHeight;
			}
			changeDivSize();
			window.onresize=function(){  
			     changeDivSize();  
			} 
	</script>
	
</body>
</html>

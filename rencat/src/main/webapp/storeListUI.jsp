<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <title>所有门店</title>
	<%@include file="common/common.jspf" %>	  
    <link href="${basePath }css/storeList.css" rel="stylesheet" type="text/css">
</head>
<body>
	<%@include file="common/top.jspf" %>	
	<%@include file="common/nav.jspf" %>

	<div class="_clearFloat"></div>
	
	<div class="main_bor">
		<div class="main_le">
	    	<div class="dqmd"><span>&nbsp;</span>&nbsp;<em>共有3个门店</em></div>
	        <div class="dqmd_01">
	        	<h2>香洲门店</h2>
	        		<p><em><img src="img/md_01.png"></em><span>门店地址：香洲街1号</span></p>
				<p><em><img src="img/md_04.png"></em><span>营业时间： 24小时服务</span></p>
	            <div class="msyd"><a href="rentalCarUI.action">马上预定</a></div>
	        </div><div class="dqmd_01">
	        	<h2>斗门门店</h2>
	        		<p><em><img src="img/md_01.png"></em><span>门店地址：斗门鳄鱼岛</span></p>
				<p><em><img src="img/md_04.png"></em><span>营业时间： 24小时服务</span></p>
	            <div class="msyd"><a href="rentalCarUI.action">马上预定</a></div>
	        </div><div class="dqmd_01">
	        	<h2>金湾门店</h2>
	        		<p><em><img src="img/md_01.png"></em><span>门店地址：吉林大学珠海学院榕园五栋</span></p>
				<p><em><img src="img/md_04.png"></em><span>营业时间： 06：00-23：30</span></p>
	            <div class="msyd"><a href="rentalCarUI.action">马上预定</a></div>
	        </div>
	    </div>
	    <div class="main_ri">
	    	<div class="dt" id="allmap"></div>
	    </div>
	</div>

	<%@include file="common/bottom.jspf" %>

<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=ETLXgCxIoVixggHcAk6mKpMd"></script>
<script>
	
	$("#storepage").append("<div  class='bdr'></div>");

    // 百度地图API功能
    var store = '[{"id":"1","area_id":"3","title":"\u6d77\u5a01\u79df\u8f66","address":"\u9999\u6d32\u8857\u0031\u53f7","open_time":"24\u5c0f\u65f6\u670d\u52a1","phone":"08983365241","bus_line":"1\u8def\uff0c2\u8def\uff0c33\u8def","add_time":"1470102430","status":"1","map":"113.532751,22.272573"},{"id":"2","area_id":"7","title":"\u4e09\u4e9a\u95e8\u5e97","address":"\u6597\u95e8\u9cc4\u9c7c\u5c9b","open_time":"24\u5c0f\u65f6","phone":"1356988578","bus_line":"\u6751","add_time":"1470279821","status":"1","map":"113.331723,22.188498"},{"id":"6","area_id":"4","title":"\u4e2d\u5c71\u5357\u5e97","address":"\u5409\u6797\u5927\u5b66\u73e0\u6d77\u5b66\u9662\u6995\u56ed\u4e94\u680b","open_time":"08\uff1a00-24\uff1a00","phone":"13138920100","bus_line":"9\u8def 42\u8def 45\u8def \u5e9c\u57ce\u4e2d\u5b66\u7ad9 ","add_time":"1473730505","status":"1","map":"113.414073,22.063767"}]';
    
    
    
    var map   = new BMap.Map("allmap");
    var point = new BMap.Point(113.414073,22.063767);
    map.centerAndZoom(point, 12);
    var points = [];
    addmarker(store); // 将标注添加到地图中
    var view = map.getViewport(eval(points));
    var mapZoom = view.zoom;
    var centerPoint = view.center;
    map.centerAndZoom(centerPoint,mapZoom);
function addmarker(data){
    var data = eval('('+data+')');
    for(var i in data){
        var d = data[i];
        var m = d['map'].split(',');
        var point = new BMap.Point(m[0],m[1]);
        points.push(point);
        map.addOverlay(new BMap.Marker(point));
    }
}
</script>
</body>
</html>
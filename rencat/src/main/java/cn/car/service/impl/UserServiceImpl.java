package cn.car.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.car.dao.UserDao;
import cn.car.entity.User;
import cn.car.service.UserService;

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements
		UserService {

	private UserDao userDao;
	@Resource
	public void setUserDao(UserDao userDao) {
		super.setBaseDao(userDao);
		this.userDao = userDao;
	}
	
	public User login(String account, String password) {
		List<User> list = userDao.findObjectByAccAndPwd(account,password);
		if(list.size()>0){
			return list.remove(0);
		}
		return null;
	}
}

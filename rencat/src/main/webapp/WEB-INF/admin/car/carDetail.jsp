<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@include file="../common.jspf" %>
		<link rel="stylesheet" type="text/css" href="${basePath }acss/carDetail.css"/>
	</head>
	<body>
		<div class="_BigBox">
			<a class="_aBtn" href="javascript:history.go(-1)">返回</a>	
			<c:if test="${ADMININFO.role }">
				<a class="_bBtn"  href="carModifyUI.action?cid=${car.id }">修改</a>
			</c:if>
			<div class="_clearFloat"></div>
			<p class="_title">车辆资料</p>
			<div class="_infoBox">
				<div class="_img">
					<img src="${car.image }" alt="" />
				</div>
				<div class="_info_l">
					车辆编号:<span>${car.id }</span>
				</div>
				<div class="_info_l">
					车辆型号:<span>${car.version }</span>
				</div>
				<div class="_info_l">
					租赁价格:<span>${car.price }元</span>
				</div>
				<div class="_info_l">
					发布时间:<span>${car.releaseTime }</span>
				</div> 
				<div class="_info">
					排档:<span>${car.gear }</span>
				</div>
				<div class="_info">
					定金:<span>${car.earnestMoney }</span>
				</div>
				<div class="_info">
					品牌:<span>${car.brand }</span>
				</div>
				<div class="_info">
					种类:<span>${car.type.name }</span>
				</div>
				<div class="_info">
					排量:<span>${car.displacement }L</span>
				</div>
				<div class="_info">
					几人座:<span style="width: 85px;">${car.seat }</span>
				</div> 
				<!--  
					<div class="_info">
						状态:<span style="color: crimson;">已租出</span>
					</div> 
				-->
				<div class="_info_l" style="width: 530px;">
					车辆描述:<span style="width: 458px;">${car.description }</span>
				</div> 
			</div>
		</div>
	</body>
</html>

<%@ page language="java"  pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <meta name="Keywords" content="车辆预订" />
    <meta name="Description" content="车辆预订" />
    <title>车辆预订</title>
	<%@include file="common/common.jspf" %>    
<link rel="stylesheet" type="text/css" href="${basePath }css/orderUI.css"/>
</head>
<body>
	<%@include file="common/top.jspf" %>
	<%@include file="common/nav.jspf" %>

<div class="main">
    <div class="ti">
        <h2>完善并核对订单信息</h2>
        <a href="javascript:history.go(-1)">返回</a>
    </div>
    <div class="bo bb">
        <div class="to">
            <div class="xx">
                <span></span><em>预订人信息</em>
            </div>
            <div class="cr">
                <h2>用户信息</h2>
                <div class="bt">
                    <div class="xm">帐号：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="username"  type="text" readonly="readonly" placeholder="${USERINFO.account }" >
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">姓名：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="username"  type="text" readonly="readonly" placeholder="${USERINFO.name }" >
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">手机号码：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="mobile" type="text" readonly="readonly" placeholder="${USERINFO.mobile }" >
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">身份证号：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="bodycode"readonly="readonly" placeholder="${USERINFO.idcard }" name="">
                    </div>
                </div>
                <input type="hidden" id="carId"  value="${car.id }"/>
            </div>
        </div>
        <div class="to">
            <div class="xx">
                <span></span><em>门店信息 </em>
            </div>
            <div class="cr jl">
                <div class="md"><span>取车门店：</span><em>${takestore.name }&nbsp;&nbsp;<b style="color: red;float: none">地址: </b>${takestore.address }</em></div>
            </div>
            <div class="cr">
                <div class="md"><span>还车门店：</span><em>${returnstore.name }&nbsp;&nbsp;<b style="color: red;float: none">地址: </b>${returnstore.address }</em></div>
            </div>
        </div>
        <div class="to">
            <div class="xx">
                <span></span><em>车辆与租期</em>
            </div>
            <div class="cr jl">
                <div class="md"><span>车辆型号：</span><em>${car.version }</em></div>
            </div>
            <div class="cr">
                <div class="md"><span>取车时间：</span><b id="taketime">2016-12-20 20:22</b><em class="jj">(取)</em><span style="padding-left: 5px;color: #1454ac;width:60px;"  onclick="showHide(1)" >修改</span></div>
            </div>
            <div class="cr" id="show_1" style="display: none;height: 45px;">
                <div class="md">
                    <span>修改时间：</span>
                    <input type="text" class="date wbk_01" style="width: 180px;" value="2016-12-20 20:22" id="date" size="37">
                    <input type="hidden" id="date1" value="0">
                </div>
            </div>
            <div class="cr">
                <div class="md"><span>还车时间：</span><b id="returntime">2016-12-21 20:22</b><em class="jj">(还)</em><span style="padding-left: 5px;color: #1454ac;width: 60px;" onclick="showHide(2)" >修改</span></div>
            </div>
            <div class="cr" id="show_2" style="display: none;height: 45px;">
                <div class="md">
                    <span>修改时间：</span>
                    <input type="text" class="date wbk_01" style="width: 180px;"  value="2016-12-21 20:22" id="date-to" size="37">
                    <input type="hidden" id="date2" value="0">
                </div>
            </div>
            <div class="cr">
                <div class="md"><span>日均租金：</span><em class="sj">${car.price }元/天 </em></div>
            </div>
        </div>
        
        <div class="to">
            <div class="xx">
                <span></span><em>支付方式</em>
            </div>
            <div class="zf">
                <label class="zf_01"><input class="mmxz" type="radio" value="1" checked="checked" name="use">到店支付</label>
                <!--<label class="zf_01"><input class="mmxz" type="radio" value="2"  name="use">支付宝支付</label>-->
            </div>
        </div>


        <div class="to">
            <div class="xx">
                <span></span><em>预授权和定金费用</em>
            </div>
            <div class="jg_bor">
                <div class="jg" style="margin-left: 0px;">
                    <b>${car.earnestMoney }元／车</b>
                    <em>( 该费用将在门店从信用卡刷取，暂不计入合计金额！)</em>
                </div>
                <div class="jg_bo">
                    <p>1. 在您取车时，我们将刷取${car.earnestMoney }元/车的预授权。</p>
                    <p>2. 在您还车后，我们会查询您用车期间是否有违章，如无违章，15天后自动解冻/退款。</p>
                    <p>3. 若您为首次租车，您必须使用信用卡刷取预授权。</p>
                </div>
            </div>
        </div>
        <div class="to">
            <div class="xx">
                <span></span><em>温馨提示</em>
            </div>
            <div class="jg_bor">
                <p class="coc">您当前取车地址为便捷点取车，为保证您正常用车，建议您提前一个小时下单，谢谢。</p>
            </div>
        </div>
        <div class="xdd">
            <div class="xdd_le">
                <label class="yd">
                    <input class="fx" type="checkbox" checked>
                    <span>我已阅读</span>
                    <a href="#">《自由族车辆租赁合同》</a>
                    <span>，并同意全部条款。</span>
                </label >
            </div>
            <div class="xdd_ri">
                <ul class="xdd_ri">
                    <li><span>车辆租金：</span><em id="carprice">${car.price }元</em></li>
                </ul>
                <div class="tjdd"><span>应付金额：</span><em t="250"  id="price">先确定租期</em></div>
                <div class="qq"></div>
                <div class="cc">
                    <input  class="cc_01" type="button" id="subtn" value="提交订单" onclick="addOrder()">
                </div>
            </div>
        </div>
    </div>
</div>
	<%@include file="common/bottom.jspf" %>
			
<script type="text/javascript" src="${basePath }js/datepicker/WdatePicker.js"></script>
<script type="text/javascript">

    $("input[name='rec_type']").click(function(){
        if($(this).val()==1){
            var v = $("#price").attr('t');
        }
        if($(this).val()==2){
            var v = $("#price").attr('dj');
        }
        $("#price").html(v+'元');
    });


    $(".date").click(function(){
        WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',minDate:'%y-%M-%d',onpicked:function(){
           var v = $dp.cal.getP('y')+'-'+$dp.cal.getP('M')+'-'+$dp.cal.getP('d')+' '+$dp.cal.getP('H')+':'+$dp.cal.getP('m');
            if($("#date1").val()==1){
                var param    = {
                    'start_time' : v
                };
            }else if($("#date2").val()==1){
                var param    = {
                    'end_time' : v
                };
            }
        }});
        
    });

    function showHide(v){
        if(v==1){
            $("#date1").val(1);
            $("#date2").val(2);
        }else{
            $("#date1").val(2);
            $("#date2").val(1);
        }
        $("#show_"+v).toggle();
      
        
        /* 淘汰的代码
        var taketime = $("#taketime").text();
        var returntime = $("#returntime").text();
      	taketime = taketime.substring(0,10);
      	returntime = returntime.substring(0,10);
        */
      	
      	var time = getDay($("#date").val(),$("#date-to").val());
      	if(time < 1){
      		alert("至少要租一天以上");
      	}else{
	      	$("#taketime").text($("#date").val());
	        $("#returntime").text($("#date-to").val());
	        var price = $("#carprice").text();
	        price = price.substring(0,price.length-1);
	        $("#price").text(time*price+"元");
	        
      	}
    }
    //根据两个日期得到间隔多少天
    function getDay(taketime,returntime){
    	var year1 = taketime.substring(0,4);
    	var year2 = returntime.substring(0,4);
    	var month1 = taketime.substring(5,7);
    	var month2 = returntime.substring(5,7);
    	var day1 = taketime.substring(8,10);
    	var day2 = returntime.substring(8,10);
    	
    	var time1 = new Date(year1,month1,day1);
    	var time2 = new Date(year2,month2,day2);
    	var time = time2-time1;
    	time = time/(1000 * 60 * 60 * 24);
    	return time;
    }
    
    /*生成订单
    */
    function addOrder(){
    	var taketime=$("#taketime").text();
    	var rt=$("#returntime").text();
    	var time = getDay(taketime,rt);
    	var cid=$("#carId").val();
		var ts = taketime.split(" ");
		window.location = "addOrder.action?time1="+ts[0]+"&time2="+ts[1]+"&cid="+cid+"&days="+time;
    	/*废弃代码
    	由于请求不能带空格,所以只能把时间参数拆成两半
    	例如 2016-12-19 22:03
    	拆成 time1=2016-12-19
    			time2=22:03
    			然后在后台再拼接起来
    			如上代码
    	$.post(
    		"addOrder.action",
    		{"tt":tt,
    			"days":time,
    		"cid":cid},
    		function(){
    			
    		}
    	);
    	alert(cid);
    	*/
    }
</script>
</body>
</html>
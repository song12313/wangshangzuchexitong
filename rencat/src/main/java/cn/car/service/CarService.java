package cn.car.service;

import java.util.List;
import java.util.Map;

import cn.car.entity.Car;

/**
 * 对外暴露的车辆业务类
 * @author xs
 */
public interface CarService extends BaseService<Car> {

	/**
	 * 根据车种类查找车辆
	 * @param typeid
	 * @return
	 */
	List<Car> findObjectByType(String typeid);

	/**
	 * 根据众多条件查找车辆
	 * @param conditions
	 * @return
	 */
	List<Car> findObjectByConditions(Map<String, String> conditions);

}

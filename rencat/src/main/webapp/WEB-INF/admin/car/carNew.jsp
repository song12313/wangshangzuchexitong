<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@include file="../common.jspf" %>
		<link rel="stylesheet" type="text/css" href="${basePath }acss/carNew.css"/>
	</head>
	<body>
		<div class="_BigBox">
			<form action="addCar.action" method="post" enctype="multipart/form-data">
				<a class="_aBtn" href="javascript:history.go(-1)">返回</a>	
				<div class="_clearFloat"></div>
				<p class="_title">用户资料</p>
				<div class="_infoBox">
					<div class="_img">
						<img src="" alt="上传汽车图片" />
					</div>
					<div class="_info_l">
						车辆编号:<input type="text" value="后台自动生成" readonly="readonly"  />
					</div>
					<div class="_info_l">
						发布时间:<input type="text" value="后台自动生成" readonly="readonly" />
					</div> 
					<div class="_info_l">
						车辆型号:<input type="text" name="version" />
					</div>
					<div class="_info_l">
						租赁价格:<input type="text" name="price" />
					</div>
					<div class="_info">
						排档:<input type="text" name="gear" />
					</div>
					<div class="_info">
						定金:<input type="text" name="earnestMoney" />
					</div>
					<div class="_info">
						品牌:<input type="text" name="brand" />
					</div>
					<div class="_info">
						排量:<input type="text" name="displacement" />
					</div>
					<div class="_info">
						几人座:<input type="text" name="seat" style="width: 80px;" />
					</div> 
					<!--  
					<div class="_info">
						状态:<input style="color: crimson;" value="已租出" readonly="readonly"></input>
					</div> 
					-->
					<div class="_info">
						种类:<select name="type.id" style="">
								<option value="t1">商务车</option>
								<option value="t2">豪华车</option>
								<option value="t3">SUV</option>
								<option value="t4">经济车</option>
							</select>
					</div>
					<div class="_info_l" style="width: 530px;">
						车辆描述:<input style="width: 453px;" name="description"></input>
					</div> 
					<div class="_info" style="width: 201px;height: 25px;">
						<input style="width: 200px;border: none;" type="file" name="file" id="" />
					</div>
					<input type="button" name="" id="" value="保存新增" class="_bBtn" onclick="doSubmit();"/>
				</div>
			</form>
		
			<script type="text/javascript">
				function doSubmit(){
					document.forms[0].submit();
				}
			</script>			
		</div>
	</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@include file="../common.jspf" %>		
		<title>用户列表</title>
		<link rel="stylesheet" href="${basePath }acss/userDetail.css" />
	</head>
	<body>
		<div class="_BigBox">
			<a class="_aBtn" href="javascript:history.go(-1)">返回</a>	
			<a class="_bBtn" href="orderListUI.action?uid=${user.id }">查看该用户订单</a>
			<div class="_clearFloat"></div>
			<p class="_title">用户资料</p>
			<div class="_infoBox">
				<div class="_img">
					<img src="${user.headimg }" alt="" />
				</div>
				<div class="_info">
					帐号:<span>${user.account }</span>
				</div>
				<div class="_info">
					密码:<span>${user.password }</span>
				</div>
				<div class="_info">
					手机:<span>${user.mobile }</span>
				</div>
				<div class="_info">
					邮箱:<span>${user.email }</span>
				</div>
				<div class="_info">
					姓名:<span>${user.name }</span>
				</div>
				<div class="_info">
					性别:<span>${user.sex }</span>
				</div>
				<div class="_info">
					身份证:<span style="width: 184px;">${user.idcard }</span>
				</div>
				<div class="_info">
					驾驶证:<span style="width: 184px;">${user.dlicense }</span>
				</div> 
				<div class="_info_l">
					帐号状态:<span style="color: crimson;">${user.state }</span>
				</div> 
				<div class="_info_l">
					驾照类型:<span>${user.dking }</span>
				</div> 
			</div>
		</div>
	</body>
</html>

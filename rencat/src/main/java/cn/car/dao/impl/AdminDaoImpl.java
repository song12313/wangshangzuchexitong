package cn.car.dao.impl;

import java.util.List;

import org.hibernate.Query;

import cn.car.dao.AdminDao;
import cn.car.entity.Admin;

public class AdminDaoImpl extends BaseDaoImpl<Admin> implements AdminDao{

	public List<Admin> findObjectByAccAndPwd(String account, String password) {
		Query query = getSession().createQuery("from Admin where account=? and password = ?");
		query.setParameter(0, account);
		query.setParameter(1, password);
		return query.list();
	}

}

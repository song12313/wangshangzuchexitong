<%@ page language="java"  pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<%@include file="../common.jspf" %>
		<link rel="stylesheet" type="text/css" href="${basePath }acss/cssReset.css"/>
		<style type="text/css">
			html{
				background-color:#ECF0F5;
			}
			body{
				padding-top: 10px;
			}
			._top{
				margin: 0 auto;
				width: 1050px;
				min-width: 1050px;
				height: 500px;
			}
			._top p{
				font-size: 26px;
				font-weight: 700;
			}
			._BigBox{
				width: 1050px;
				height: 500px;
				border: 2px solid gainsboro;
				border-radius: 5px;
			}
			._BoxLet{
				width: 50%;
				height: 100%;
				float: left;
			}
			._BoxRight{
				width: 50%;
				height: 100%;
				float: left;
			}
		</style>
	</head>
	<body>
		<div class="_top">
			<p>热门租借车型,城市</p>
			<div class="_BigBox">
				<div class="_BoxLet">
					<div id="chart1">FusionCharts XT will load here!</div>
				</div>
				<div class="_BoxRight">
					<div id="chart2">FusionCharts XT will load here!</div>
				</div>
			</div>
		</div>
		
		<script type="text/javascript" src="${basePath }ajs/fusioncharts.js" ></script>
		<script type="text/javascript" src="${basePath }ajs/fusioncharts.charts.js" ></script>
		<script>
			 FusionCharts.ready(function(){
			    var fusioncharts = new FusionCharts({
			    type: 'pie2d',
			    renderAt: 'chart1',
			    width: '100%',
			    height: '100%',
			    dataFormat: 'json',
			    dataSource: {
			        "chart": {
			            "caption": "品牌",
			            "subCaption": "2016年",
		                "numberPrefix": "$",
			            "showPercentValues": "1",
			            "showPercentInTooltip": "0",
			            "decimals": "1",
			            "use3DLighting": "0",
			            "useDataPlotColorForLabels": "1",
			            //Theme
			            "theme": "fint"
			        },
			        "data": [{
			            "label": "宝马",
			            "value": "385"
			        }, {
			            "label": "广汽丰田",
			            "value": "246"
			        }, {
			            "label": "东风标志",
			            "value": "105"
			        }, {
			            "label": "其他",
			            "value": "291"
			        }]
			    }
			}
			);
			    fusioncharts.render();
			});
			
			 FusionCharts.ready(function(){
			    var fusioncharts = new FusionCharts({
			    type: 'pie2d',
			    renderAt: 'chart2',
			    width: '100%',
			    height: '100%',
			    dataFormat: 'json',
			    dataSource: {
			        "chart": {
			            "caption": "门店租赁订单数",
			            "subCaption": "2016年",
		                "numberPrefix": "$",
			            "showPercentValues": "1",
			            "showPercentInTooltip": "0",
			            "decimals": "1",
			            "use3DLighting": "0",
			            "useDataPlotColorForLabels": "1",
			            //Theme
			            "theme": "fint"
			        },
			        "data": [{
			            "label": "金湾店",
			            "value": "285"
			        }, {
			            "label": "斗门店",
			            "value": "146"
			        }, {
			            "label": "香洲店",
			            "value": "105"
			        }, {
			            "label": "其他",
			            "value": "491"
			        }]
			    }
			}
			);
			    fusioncharts.render();
			});
		</script>
	</body>
</html>

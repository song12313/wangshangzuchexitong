package cn.car.dao.impl;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;

import cn.car.dao.OrderDao;
import cn.car.entity.Order;

public class OrderDaoImpl extends BaseDaoImpl<Order> implements OrderDao {

	public List<Order> findObjectByUserId(String userid) {
		Query query = getSession().createQuery("from Order where userid = ? order by ordertime desc");
		query.setParameter(0, userid);
		return query.list();
	}

	public List<Order> findObjectByState(String state) {
		Query query = getSession().createQuery("from Order where state = ? order by ordertime desc");
		query.setParameter(0, state);
		return query.list();
	}

	public void changeOrderState(String sid, String state) {
		SQLQuery query = getSession().createSQLQuery("UPDATE t_order SET state = ? WHERE id = ?");
		query.setParameter(0, state);
		query.setParameter(1, sid);
		query.executeUpdate();
	}

	public List<Order> findObjectByStateAndUser(String state, String uid) {
		Query query = getSession().createQuery("from Order where state = ? and userid = ? order by ordertime desc");
		query.setParameter(0, state);
		query.setParameter(1, uid);
		return query.list();
	}


}

<%@ page language="java"  pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <title>自由族</title>
    <%@include file="common/common.jspf" %>
	<link rel="stylesheet"  href="${basePath }css/carDetail.css"/>
</head>
<body>

	<%@include file="common/top.jspf" %>
	<%@include file="common/nav.jspf" %>


<div class="._clearFloat"></div>
<div class="main">
    <div class="main_bor">
        <div class="main_le">
            <div class="cm">车辆信息</div>
            <div class="cz">
                <div class="cz_le"><img src="${car.image }"></div>
            </div>
            <div class="jbxx">
                <div class="cm">车辆信息</div>
                <table class="ta">
                    <tr class="tr_01">
                        <td class="tr_02">品　　牌：</td>
                        <td class="tr_03">${car.brand }</td>
                        <td class="tr_02">车辆型号：</td>
                        <td class="tr_03">${car.version }</td>
                    </tr>
                    <tr class="tr_01">
                        <td class="tr_02">类　　型：</td>
                        <td class="tr_03">${car.type.name }</td>
                        <td class="tr_02">租赁价格：</td>
                        <td class="tr_03">${car.price }元/天</td>
                    </tr>
                    <tr class="tr_01">
                        <td class="tr_02">座&nbsp;位&nbsp;数：</td>
                        <td class="tr_03">${car.seat }座</td>
                        <td class="tr_02">排　　量： </td>
                        <td class="tr_03">${car.displacement }L</td>
                    </tr>
                    <tr class="tr_01">
                        <td class="tr_02">排　　档：</td>
                        <td class="tr_03">${car.gear }</td>
                        <td class="tr_02">发布时间： </td>
                        <td class="tr_03">${car.releaseTime }</td>
                    </tr>
                </table>
            </div>
            <a href="orderUI.action?cid=${car.id }" class="msyd">租这一辆</a>
        </div>
        
        
        <div class="main_ri">
            <div class="ri_title">推荐车辆</div>
	            <div class="tjzq">
	                <ul>
	                    <li>
	                        <a href="carDetailUI.action?cid=8a9e67ad590d7dd301590d9479090008"><img src="${basePath }upload/car/1481991551239.png"></a>
	                        <div class="jg"><a href="#"><span>720.0元/天</span></a></div>
	                    </li>
	                </ul>
	                <ul>
	                    <li>
	                        <a href="carDetailUI.action?cid=8a9e67ad590d7dd301590d9366880007"><img src="${basePath }upload/car/1481991480966.png"></a>
	                        <div class="jg"><a href="#"><span>500元/天</span></a></div>
	                    </li>
	                </ul>
	                <ul>
	                    <li>
	                        <a href=carDetailUI.action?cid=8a9e67ad590d7dd301590d8e22880005"><img src="${basePath }upload/car/1481991135878.png"></a>
	                        <div class="jg"><a href="#"><span>250元/天</span></a></div>
	                    </li>
	                </ul>
	                <div class="tjzq_01">
	                    <a href="#"><img src=""></a>
	                    <span><a href="rentalCarUI.action">更多车型</a></span>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	
	<%@include file="common/bottom.jspf" %>	
	
	<script type="text/javascript">
			$("#rentalpage").append("<div  class='bdr'></div>");
	</script>
</body>
</html>
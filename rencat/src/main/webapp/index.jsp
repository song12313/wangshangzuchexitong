<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>吉珠租车</title>
		<%@include file="common/common.jspf" %>
		<link rel="stylesheet" href="${basePath }css/index.css" />
	</head>
	<body>
		<%@include file="common/top.jspf" %>
		
		<%@include file="common/nav.jspf" %>
		
		<div id="slider">
			<div class="_search">
				<form action="">
					<div class="_search_top">
						<a href="#">短租预订</a>
						<a class="_serchActive" href="#">长租预定</a>
						<a class="_serchActive" href="#">车型搜索</a>
					</div>
					<div class="_search_left">
						<p>取车</p>
						<div class="_search_left_bot">
							<select name="">
								<option value="">珠海</option>
							</select>
							<select name="">
								<option value="">香洲区</option>
								<option value="">斗门区</option>
								<option value="">金湾区</option>
							</select>
						</div>
						<div class="_search_left_bot">
							<select style="width: 210px;" name="">
								<option value="">香洲门店</option>
							</select>
						</div>
						<div class="_search_left_bot">
							<input type="text" readonly="readonly" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',minDate:'%y-%M-%d %H:%m'})" id="date" size="27">
						</div>
					</div>
					<div class="_search_left">
						<p>还车</p>
						<div class="_search_left_bot">
							<select name="">
								<option value="">珠海</option>
							</select>
							<select name="">
								<option value="">香洲区</option>
								<option value="">斗门区</option>
								<option value="">金湾区</option>
							</select>
						</div>
						<div class="_search_left_bot">
							<select style="width: 210px;" name="">
								<option value="">香洲门店</option>
							</select>
						</div>
						<div class="_search_left_bot">
							<input type="text" readonly="readonly" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',minDate:'%y-%M-%d'})" id="date" size="27">
						</div>
					</div>
					<div class="_clearFloat"></div>
					<a class="_Btn" href="rentalCarUI.action">立即选车</a>
				</form>
			</div>
			
			
			
			<ul class="slides clearfix">
				<li><img class="responsive" src="img/bg1.jpg"></li>
				<li><img class="responsive" src="img/bg2.jpg"></li>
				<li><img class="responsive" src="img/bg3.jpg"></li>
			</ul>
			<ul class="controls">
				<li><img src="" alt="previous"></li>
				<li id="next"><img src="" alt="next"></li>
			</ul>
		</div>
		
		
		<div id="_aboutme" class="aboutme">
			<div class="aboutme_left">
				<h1>自由族是谁？<span style="color: #E9E9E9;">About us</span></h1>
				<p class="_space">自由族租车（www.ziyouzu.com，隶属于自由族(珠海)科技有限责任公司）是珠海第一家P2P在线租车服务平台，目前已成为珠海最大最知名的一家汽车租赁公司，被租友称为实惠便捷的租车公司、便宜的租车公司等！</p><br />
				<p class="_space">自由族租车是一个基于互联网技术的便捷的汽车租赁共享租车服务平台，为乐于共享爱车的车主提供通过出租爱车带来可观收益的私家车出租、汽车出租服务，让爱车的闲置时间变成丰厚的现金。同时，PP租车为有租车需求的用户提供便捷、低价、便宜的就近个人租车、租车自驾、短期长期日租车、婚庆租车、结婚租车、旅游租车、商务租车、长途租车等租车服务。2012年5月，PP租车公司于（www.icarsclub.com）新加坡开始筹备运营。2013年8月，具有浓厚中国大陆背景的PP租车公司团队带着一腔热情在中国大陆成功推行自由族汽车租赁平台，正式完成在亚洲金融中心新加坡和中国首都的布局。</p><br />
				<p class="_space">自由族租车为社会创造价值，让人们出行更便利， 让汽车的使用率变得更好，为保护环境而出一份力。自由族租车团队正在不懈努力，期待以更快的速度将P2P租车服务尽快推广到全中国以及亚洲的其他地区，以期为广大用户创造更大价值，并产生良好的社会效应。 PP租车秉承着“人人信赖的汽车共享在线平台”的企业愿望，追崇“用户为至高导向”的价值观，依托人们对飞速发展的城市交通的需求，积极拓展互联网技术创新，致力于为用户提供“安全、便捷、可靠”的汽车租赁在线共享服务。</p>	
			</div>
			<div class="aboutme_map">
				<img src="img/map.png" alt="" />
			</div>
		</div>
		<div id="_serve" class="serve">
			<img src="img/servie.png" alt="" />
		</div>
		
		<div id="_flow" class="flow">
			<img src="img/flow.png" alt="" />
		</div>
		
		
		<%@include file="common/bottom.jspf" %>
		
		<script type="text/javascript" src="js/easySlider.min.js" ></script>
		<script type="text/javascript" src="js/datepicker/WdatePicker.js" ></script>
		<script type="text/javascript">
		$("#indexpage").append("<div  class='bdr'></div>");
		
			$(function() {
				$("#slider").easySlider( {
					slideSpeed: 500,
					paginationSpacing: "15px",
					paginationDiameter: "12px",
					paginationPositionFromBottom: "20px",
					slidesClass: ".slides",
					controlsClass: ".controls",
					paginationClass: ".pagination"					
				});
			});
			
			function remainTime(){  
				$('#next').click();
				setTimeout("remainTime()",3000);  
			}  
			remainTime();
			
			//滑动到锚点
			function toMao(id){
				$('html,body').animate(
					{scrollTop: ($("#"+id).offset().top)-90},
					500);
			}
		</script>
	</body>
</html>


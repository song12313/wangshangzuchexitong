<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>车辆列表</title>
		<%@include file="../common.jspf" %>		
		<link rel="stylesheet" href="${basePath }acss/carList.css"/>	
			
	</head>
	<body>
		<div class="_BigBox">
			<p>车辆列表</p>
			<select id="_type" class="_select" name="type" onchange="typeChange()">
				<option value="t1">商务车</option>
				<option value="t2">豪华车</option>
				<option value="t3">SUV</option>
				<option value="t4">经济车</option>
			</select>
			<c:if test="${ADMININFO.role }">
				<a class="_new" href="carNewUI.action">新增</a>
			</c:if>
			<div class="_clearFloat"></div>
			<div class="_table">
				<table id="carTable" border="" cellspacing="" cellpadding="">
					<tr>
						<th>车辆型号</th>
						<th>品牌</th>
						<th>租金</th>
						<th>定金</th>
						<th>发布时间</th>
						<th>种类</th>
						<th>操作</th>
					</tr>
					<c:forEach var="car" items="${cars}">
						<tr>
							<td>${car.version }</td>
							<td>${car.brand }</td>
							<td>${car.price }</td>
							<td>${car.earnestMoney }</td>
							<td>${car.releaseTime}</td>
							<td>${car.type.name }</td>
							<td><a href="carDetailUI.action?cid=${car.id }">查看</a>&nbsp;
								<c:if test="${ADMININFO.role }">
									<a href="carModifyUI.action?cid=${car.id }">修改</a>
								</c:if>	
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		
		<script type="text/javascript">
				//根据车种类异步获取车辆集合
				function typeChange(){
					var id = $("#_type option:selected").val();
					$.post(
						"ajaxGetCars.action",
						{
							typeid:id
						},
						function(data){
							var table = $("#carTable");
							table.empty();
							table.append("<tr><th>车辆型号</th><th>品牌</th><th>租金</th><th>定金</th><th>发布时间</th><th>种类</th><th>操作</th></tr>");
							$.each(data.cars,function(e,v){
								var tr = $("<tr></tr>");
								var td1 = $("<td>"+v.version+"</td>");
								var td2 = $("<td>"+v.brand+"</td>");
								var td3 = $("<td>"+v.price+"</td>");
								var td4 = $("<td>"+v.earnestMoney+"</td>");
								var td5 = $("<td>"+v.releaseTime+"</td>");
								var td6 = $("<td>"+v.type.name+"</td>");
								var td7 = $("<td></td>");
								var a1 = $("<a href='carDetailUI.action?cid="+v.id+"'>查看</a>");
								td7.append(a1);
								tr.append(td1);
								tr.append(td2);
								tr.append(td3);
								tr.append(td4);
								tr.append(td5);
								tr.append(td6);
								tr.append(td7);
								table.append(tr);
							});
						}
					);				
				}
		</script>
	</body>
</html>

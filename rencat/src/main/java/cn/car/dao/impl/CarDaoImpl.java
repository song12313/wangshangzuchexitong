package cn.car.dao.impl;

import java.util.List;

import org.hibernate.Query;

import cn.car.dao.CarDao;
import cn.car.entity.Car;

public class CarDaoImpl extends BaseDaoImpl<Car> implements CarDao {

	public List<Car> findObjectByTypeId(String typeid) {
		Query query = getSession().createQuery("from Car where typeId = ?");
		query.setParameter(0, typeid);
		return query.list();
	}

	public List<Car> findObjectByCondition(String condition) {
		Query query = getSession().createQuery("from Car "+condition);
		return query.list();
	}

}

package cn.car.web.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cn.car.entity.User;
import cn.car.service.UserService;

@Controller
public class UserAction {

	@Resource
	private UserService userService;
	
	@RequestMapping("/loginUI")
	public String toLoginUI(){
		return "/loginUI.jsp";
	}
	
	@RequestMapping("/registerUI")
	public String toRegisterUI(){
		return "/registerUI.jsp";
	}
	
	@RequestMapping("/userInfoUI")
	public String toUserInfoUI(){
		
		return "/userInfo.jsp";
	}
	
	@RequestMapping("/login")
	public String login(String account,String password,HttpSession session,HttpServletRequest request){
		User user = userService.login(account,password);
		if(user!=null){
			session.setAttribute("USERINFO", user);
			return "/index.jsp";
		}else{
			request.setAttribute("msg", "帐号或者密码错误");
			return "/loginUI.jsp";
		}
	}
	
	@RequestMapping("/register")
	public String register(User user,String valideCode,HttpSession session,HttpServletRequest request){
		try{
			String code = (String) session.getAttribute("VerifyCode");
			code = code.replaceAll(" ", "");
			if(code.equals(valideCode)){
				userService.save(user);
			}else{
				request.setAttribute("errorIdx", "valideCode");
				return "/registerUI.jsp";
			}
		}catch (Exception e) {
			request.setAttribute("errorIdx", "account");
			return "/registerUI.jsp";
		}
		return "/loginUI.jsp";
	}
	
	@RequestMapping("/quit")
	public String quit(HttpSession session){
		session.removeAttribute("USERINFO");
		return "/loginUI.jsp";
	}
	
	/**
	 * 完善个人信息的方法
	 * @param user
	 * @param file
	 * @return
	 */
	@RequestMapping("/modify")
	public String modify(User user,HttpServletRequest request,@RequestParam(value ="file",required = false) MultipartFile file,
			HttpSession session){
		try{
			String path = request.getSession().getServletContext().getRealPath("upload/user");
			String fileName = file.getOriginalFilename();
			fileName = System.currentTimeMillis()+fileName.substring(fileName.lastIndexOf("."));
			File targetFile = new File(path,fileName);
			//保存
			file.transferTo(targetFile);
			user.setHeadimg(request.getContextPath()+"/upload/user/"+fileName);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		 * 为了防止页面过来的数据覆盖了密码
		 */
		User userInfo = (User) session.getAttribute("USERINFO");
		user.setPassword(userInfo.getPassword());
		user.setState(true);
		userService.update(user);
		session.setAttribute("USERINFO", userService.findObjectById(user.getId()));
		return "/userInfo.jsp";
	}
	
	@RequestMapping("/modifyPwd.action")
	public String modifyPwd(String oldPwd,String newPwd,HttpSession session,Model model){
		User userinfo = (User) session.getAttribute("USERINFO");
		//密码正确
		if(userinfo.getPassword().equals(oldPwd)){
			userinfo.setPassword(newPwd);
			userService.update(userinfo);
			model.addAttribute("msg","密码修改成功！");
		}else{
			model.addAttribute("msg","旧密码错误,请重新输入！!");
		}
		return "/userPwdUI.jsp";
	}
	
}

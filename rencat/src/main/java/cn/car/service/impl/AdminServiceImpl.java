package cn.car.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.car.dao.AdminDao;
import cn.car.entity.Admin;
import cn.car.service.AdminService;

@Service("adminService")
public class AdminServiceImpl extends BaseServiceImpl<Admin> implements
		AdminService {

	private AdminDao adminDao;
	@Resource
	public void setAdminDao(AdminDao adminDao) {
		super.setBaseDao(adminDao);
		this.adminDao = adminDao;
	}
	
	public Admin login(String account, String password) {
		List<Admin>list =adminDao.findObjectByAccAndPwd(account,password); 
		if(list.size()>0){
			return list.remove(0); 
		}else{
			return null;
		}
	}
}

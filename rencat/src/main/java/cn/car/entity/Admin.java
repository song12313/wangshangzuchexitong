package cn.car.entity;

import java.io.Serializable;

public class Admin implements Serializable{

	private String id;
	
	private String account;
	
	private String password;
	
	private String name;
	
	private boolean role;	//是否超级管理员,false为普通管理员

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isRole() {
		return role;
	}

	public void setRole(boolean role) {
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Admin(String id, String account, String password, String name,boolean role) {
		this.id = id;
		this.account = account;
		this.password = password;
		this.name = name;
		this.role = role;
	}

	public Admin() {
		// TODO Auto-generated constructor stub
	}
	
	
}

package cn.car.service;

import cn.car.entity.User;

public interface UserService extends BaseService<User> {

	/**
	 *  登录方法
	 * @param account
	 * @param password
	 * @return
	 */
	User login(String account, String password);

}

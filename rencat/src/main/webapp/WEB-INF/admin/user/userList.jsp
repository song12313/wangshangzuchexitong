<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@include file="../common.jspf" %>		
		<title>用户列表</title>
		<link rel="stylesheet" href="${basePath }acss/userList.css" />
	</head>
	<body>
		<div class="_BigBox">
			<p>用户列表</p>
			<div class="_table">
				<table border="" cellspacing="" cellpadding="">
					<tr>
						<th>account</th>
						<th>mobile</th>
						<th>email</th>
						<th>name</th>
						<th>sex</th>
						<th>state</th>
						<th>操作</th>
					</tr>
					<c:forEach var="user" items="${users}">
						<tr>
							<td>${user.account}</td>
							<td>${user.mobile }</td>
							<td>${user.email}</td>
							<td>${user.name}</td>
							<td>${user.sex}</td>
							<td>${user.state==true?'已认证':'未认证'}</td>
							<td><a href="userDetailUI.action?uid=${user.id }">查看</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</body>
</html>
package cn.car.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import cn.car.dao.CarDao;
import cn.car.entity.Car;
import cn.car.service.CarService;

@Service("carService")
public class CarServiceImpl extends BaseServiceImpl<Car> implements CarService {

	private CarDao carDao;
	@Resource
	public void setCarDao(CarDao carDao) {
		super.setBaseDao(carDao);
		this.carDao = carDao;
	}
	public List<Car> findObjectByType(String typeid) {
		return carDao.findObjectByTypeId(typeid);
	}
	
	/**
	 * 这个方法根据条件map过来拼接成sql语句
	 */
	public List<Car> findObjectByConditions(Map<String, String> conditions) {
		String condition ="where 1=1";
		for(Map.Entry<String, String> e:conditions.entrySet()){
			String key = e.getKey();
			String value = e.getValue();
			//如果值=b,不用在sql语句中加入此条件
			if(!"b".equals(value)){
				if(!"price".equals(key)){
					condition =condition +" and "+key+"= '"+value+"'";
				}else{
					//价钱条件		0<price<200
					Integer price = Integer.parseInt(value);
					int tprice = price-200;
					condition =condition +" and "+key+"< '"+value + "' and "+key+">'"+tprice+"'";
				}
			}
		}
		return carDao.findObjectByCondition(condition);
	}
}

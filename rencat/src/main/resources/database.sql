/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.6.25 : Database - rentcar
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rentcar` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `rentcar`;

/*Table structure for table `t_admin` */

DROP TABLE IF EXISTS `t_admin`;

CREATE TABLE `t_admin` (
  `id` varchar(32) NOT NULL,
  `account` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `role` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_admin` */

insert  into `t_admin`(`id`,`account`,`password`,`name`,`role`) values ('a1','root','root','小松',''),('a2','admin','admin','普通管理员','\0');

/*Table structure for table `t_car` */

DROP TABLE IF EXISTS `t_car`;

CREATE TABLE `t_car` (
  `id` varchar(32) NOT NULL,
  `version` varchar(20) NOT NULL,
  `gear` varchar(10) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `earnestMoney` double DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `displacement` double DEFAULT NULL,
  `releaseTime` date DEFAULT NULL,
  `seat` int(11) DEFAULT NULL,
  `typeId` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68F55893939D1BD` (`typeId`),
  CONSTRAINT `FK68F55893939D1BD` FOREIGN KEY (`typeId`) REFERENCES `t_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_car` */

insert  into `t_car`(`id`,`version`,`gear`,`description`,`price`,`earnestMoney`,`image`,`brand`,`displacement`,`releaseTime`,`seat`,`typeId`) values ('8a9e67ad590d7dd301590d8661980000','宝马3系','自动排','宝马车',620,20000,'/rentcar/upload/car/1481990627729.png','宝马',2,'2016-12-18',5,'t2'),('8a9e67ad590d7dd301590d8864170001','大众途观','自动挡','大众途观',314,15000,'/rentcar/upload/car/1481990759446.png','大众',1.8,'2016-12-18',5,'t3'),('8a9e67ad590d7dd301590d89c5710002','2016别克新昂科拉','自动档','2016别克新昂科拉',192,10000,'/rentcar/upload/car/1481990849904.png','别克',1.4,'2016-12-18',5,'t3'),('8a9e67ad590d7dd301590d8ad8200003','三菱帕杰罗劲畅','自动档','三菱帕杰罗劲畅',437,19000,'/rentcar/upload/car/1481990920223.png','三菱',3,'2016-12-18',5,'t3'),('8a9e67ad590d7dd301590d8d3a020004','丰田凯美瑞','自动排','商务车',239,5000,'/rentcar/upload/car/1481991076352.png','丰田',2,'2016-12-18',5,'t1'),('8a9e67ad590d7dd301590d8e22880005','别克新君越','手动挡','别克新君越,新车来的',286,14000,'/rentcar/upload/car/1481991135878.png','别克',2.4,'2016-12-18',4,'t1'),('8a9e67ad590d7dd301590d8f55ee0006','大众帕萨特','自动挡','大众帕萨特,商务车',311,12000,'/rentcar/upload/car/1481991214573.png','大众',1.8,'2016-12-18',4,'t1'),('8a9e67ad590d7dd301590d9366880007','捷豹XE','自动档','第二辆豪华车',690,20000,'/rentcar/upload/car/1481991480966.png','捷豹',3.2,'2016-12-18',5,'t2'),('8a9e67ad590d7dd301590d9479090008','沃尔沃XC-Classic','自动档','第三辆豪华车',720,21000,'/rentcar/upload/car/1481991551239.png','沃尔沃',4,'2016-12-18',7,'t2'),('8a9e67ad590d7dd301590d9886d00009','现代朗动s','自动档','第一辆经济车',100,4000,'/rentcar/upload/car/1481991816911.png','现代',1.2,'2016-12-18',5,'t4'),('8a9e67ad590d7dd301590d995fc7000a','福特福克斯','手动挡','福特福克斯',110,4500,'/rentcar/upload/car/1481991872454.png','福特',1.4,'2016-12-18',5,'t4'),('8a9e67ad590d7dd301590d9a3149000b','本田飞度s01','手动挡','本田飞度s01',90,3400,'/rentcar/upload/car/1481991926088.png','本田',1.1,'2016-12-18',4,'t4');

/*Table structure for table `t_order` */

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `id` varchar(32) NOT NULL,
  `days` int(11) NOT NULL,
  `ordertime` datetime DEFAULT NULL,
  `realtime` datetime DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `carid` varchar(32) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `takestoreid` varchar(32) DEFAULT NULL,
  `returnstoreid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA0C0C3C3B654E07F` (`returnstoreid`),
  KEY `FKA0C0C3C3D7938D55` (`carid`),
  KEY `FKA0C0C3C33A95AA9F` (`userid`),
  KEY `FKA0C0C3C3AC5F4948` (`takestoreid`),
  CONSTRAINT `FKA0C0C3C33A95AA9F` FOREIGN KEY (`userid`) REFERENCES `t_user` (`id`),
  CONSTRAINT `FKA0C0C3C3AC5F4948` FOREIGN KEY (`takestoreid`) REFERENCES `t_store` (`id`),
  CONSTRAINT `FKA0C0C3C3B654E07F` FOREIGN KEY (`returnstoreid`) REFERENCES `t_store` (`id`),
  CONSTRAINT `FKA0C0C3C3D7938D55` FOREIGN KEY (`carid`) REFERENCES `t_car` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_order` */

insert  into `t_order`(`id`,`days`,`ordertime`,`realtime`,`state`,`description`,`carid`,`userid`,`takestoreid`,`returnstoreid`) values ('8a9e1e9f5917a63d015917a6ed890000',2,'2016-12-19 23:15:32','2016-12-20 20:22:00','finish',NULL,'8a9e67ad590d7dd301590d8d3a020004','8a9ec34a591632980159163389070000','m2','m1'),('8a9e4622591a5f6901591a603b2b0000',2,'2016-12-20 11:57:11','2016-12-20 20:22:00','wait',NULL,'8a9e67ad590d7dd301590d9886d00009','8a9ec34a591632980159163389070000','m2','m1'),('8a9e4622591b903b01591b9b7ea00000',15,'2016-12-20 17:41:32','2016-12-20 20:22:00','wait',NULL,'8a9e67ad590d7dd301590d9479090008','8a9e67ad590cad6501590cb5bfc90001','m3','m3');

/*Table structure for table `t_store` */

DROP TABLE IF EXISTS `t_store`;

CREATE TABLE `t_store` (
  `id` varchar(32) NOT NULL,
  `name` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_store` */

insert  into `t_store`(`id`,`name`,`address`,`phone`,`description`) values ('m1','金湾门店','吉林大学珠海学院','13631276000',NULL),('m2','香洲门店','香洲大街1号','21323232',NULL),('m3','斗门门店','鳄鱼岛1号','213424',NULL);

/*Table structure for table `t_type` */

DROP TABLE IF EXISTS `t_type`;

CREATE TABLE `t_type` (
  `id` varchar(32) NOT NULL,
  `name` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_type` */

insert  into `t_type`(`id`,`name`) values ('t1','商务车'),('t2','豪华车'),('t3','SUV'),('t4','经济车');

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` varchar(32) NOT NULL,
  `account` varchar(16) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `email` varchar(16) DEFAULT NULL,
  `idcard` varchar(18) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `dlicense` varchar(20) DEFAULT NULL,
  `dking` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `headimg` varchar(255) DEFAULT NULL,
  `state` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`account`,`password`,`mobile`,`email`,`idcard`,`name`,`sex`,`dlicense`,`dking`,`address`,`headimg`,`state`) values ('8a9e67ad590cad6501590cb5bfc90001','test','test','4448123','test@qq.com','123123213123','test','女','test123123213','test123123213','test123123213','/rentcar/upload/user/1481982340099.jpg',''),('8a9ec34a591632980159163389070000','xiaosong','xiaosong','1322132','444812313@qq.com','1242414223','翁小松','男','1242414223','小车','珠海吉林大学珠海学院','/rentcar/upload/user/1482150439920.jpg','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;



package cn.car.dao;

import java.util.List;

import cn.car.entity.User;

public interface UserDao extends BaseDao<User> {

	/**
	 * 根据帐号和密码查找用户
	 * @param account
	 * @param password
	 * @return
	 */
	List<User> findObjectByAccAndPwd(String account, String password);

}

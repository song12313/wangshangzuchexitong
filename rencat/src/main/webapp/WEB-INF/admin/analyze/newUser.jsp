<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<%@include file="../common.jspf" %>		
		<link rel="stylesheet" href="${basePath }acss/cssReset.css" />
		<style>
			html{
				background-color:#ECF0F5;
			}
			body{
				padding-top: 10px;
			}
			._top{
				margin: 0 auto;
				width: 90%;
				min-width: 930px;
				height: 180px;
			}
			._top p{
				font-size: 26px;
				font-weight: 700;
			}
			._top_cont{
				border: 1px solid gray;
				width: 100%;
				min-width: 930px;
				height: 100px;
				border-radius: 5px;
				background-color: rgba(255,250,240,1);
				overflow: hidden;
			}
			._infoBox{
				width: 110px;
				height: 50px;
				border: 1px solid gray;
				float: left;
				margin: 25px 20px;
			}
			._infoBox_top{
				border-bottom: 1px solid gray;
				height: 24px;
				text-align: center;
				line-height: 24px;
				font-weight: 500;
			}
			._infoBox_context{
				text-align: center;
				height: 25px;
				line-height: 25px;
				background-color: white;
			}
			/*-----------------顶部完成-----------------*/
			
			._chart{
				margin: 0 auto;
				width: 90%;
				min-width: 930px;
				/*height: 500px;*/
				border:1px solid gray;
				border-radius: 5px;
				overflow: hidden;
			}
			
		</style>
	</head>
	<body>
		<div class="_top">
			<p>新增用户</p>	
			<div class="_top_cont">
				
				<div class="_infoBox">
					<div class="_infoBox_top">总用户</div>
					<div class="_infoBox_context"><span>21312</span></div>
				</div>
				
				<div class="_infoBox">
					<div class="_infoBox_top">昨日新增</div>
					<div class="_infoBox_context"><span>11</span></div>
				</div>
				
				<div class="_infoBox">
					<div class="_infoBox_top">今日新增</div>
					<div class="_infoBox_context"><span>12</span></div>
				</div>
				
				<div class="_infoBox">
					<div class="_infoBox_top">上月新增</div>
					<div class="_infoBox_context"><span>122</span></div>
				</div>
				
				<div class="_infoBox">
					<div class="_infoBox_top">今月新增</div>
					<div class="_infoBox_context"><span>122</span></div>
				</div>
				
				<div class="_infoBox">
					<div class="_infoBox_top">月同比</div>
					<div class="_infoBox_context"><span>0.2</span></div>
				</div>
				
			</div>
		</div>
	
		<div class="_chart">
			<div id="chartContainer">FusionCharts XT will load here!</div>
		</div>

		<script type="text/javascript" src="${basePath }ajs/fusioncharts.js" ></script>
		<script type="text/javascript" src="${basePath }ajs/fusioncharts.charts.js" ></script>
		<script>
		FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "line",
        "renderAt": "chartContainer",
        "width": "100%",
        "height": "500px",
        
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption": "每日注册数",
              "xAxisName": "一周",
              "theme": "fint"
           },
          "data": [
              {
                 "label": "星期日",
                 "value": "220"
              },
               {
                 "label": "星期一",
                 "value": "420"
              },
               {
                 "label": "星期二",
                 "value": "490"
              },
               {
                 "label": "星期三",
                 "value": "420"
              },
               {
                 "label": "星期四",
                 "value": "120"
              },
               {
                 "label": "星期五",
                 "value": "220"
              },
               {
                 "label": "星期六",
                 "value": "320"
              }
           ]
        }
    });

    revenueChart.render();
})
		</script>
	</body>
</html>

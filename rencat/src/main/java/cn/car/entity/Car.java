package cn.car.entity;

import java.io.Serializable;
import java.util.Date;

public class Car implements Serializable {

	private String id;
	private String version;				//型号
	private String description;		
	private String gear;	//自动挡还是手动挡
	private double price;				//租金
	private double earnestMoney;	//定金
	private String image;
	private String brand;				//品牌
	private double displacement;	//排量
	private Date releaseTime;			//发行时间
	private int seat;	//几人座
	private Type type;
	
	
	public Car() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGear() {
		return gear;
	}
	public void setGear(String gear) {
		this.gear = gear;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getEarnestMoney() {
		return earnestMoney;
	}
	public void setEarnestMoney(double earnestMoney) {
		this.earnestMoney = earnestMoney;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public double getDisplacement() {
		return displacement;
	}
	public void setDisplacement(double displacement) {
		this.displacement = displacement;
	}
	public Date getReleaseTime() {
		return releaseTime;
	}
	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}
	public int getSeat() {
		return seat;
	}
	public void setSeat(int seat) {
		this.seat = seat;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
	
}

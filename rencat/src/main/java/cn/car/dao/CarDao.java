package cn.car.dao;

import java.util.List;

import cn.car.entity.Car;

public interface CarDao extends BaseDao<Car> {

	List<Car> findObjectByTypeId(String typeid);

	
	List<Car> findObjectByCondition(String condition);
		
}

package cn.car.service.impl;

import java.io.Serializable;
import java.util.List;

import cn.car.dao.BaseDao;
import cn.car.service.BaseService;

public class BaseServiceImpl<T> implements BaseService<T> {

	private BaseDao<T> baseDao;
	
	public void setBaseDao(BaseDao<T> baseDao) {
		this.baseDao = baseDao;
	}

	public void save(T entity) {
		baseDao.save(entity);
	}

	public void delete(Serializable id) {
		baseDao.delete(id);
	}

	public void update(T entity) {
		baseDao.update(entity);
	}

	public T findObjectById(Serializable id) {
		return baseDao.findObjectById(id);
	}

	public List<T> findObjects() {
		return baseDao.findObjects();
	}

}

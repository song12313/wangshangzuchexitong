<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<title>订单列表</title>
		<%@include file="../common.jspf" %>
		<link rel="stylesheet" href="${basePath }acss/orderList.css" />
	</head>
	<body>
		<div class="_BigBox">
			<p>
			<c:if test="${!empty uid }">用户的</c:if>订单列表</p>
			<select  id="_state" class="_select" onchange="stateChange()">
				<option value="wait">待提车</option>
				<option value="renting">租赁中</option>
				<option value="finish">已完成</option>
				<option value="invalid">无效</option>
			</select>
			<div class="_table">
				<table  id="orderTable"  border="" cellspacing="" cellpadding="">
					<tr>
						<th>订单编号</th>
						<th>订单时间</th>
						<th>租用天数</th>
						<th>用户</th>
						<th>车辆型号</th>
						<th>取车门店</th>
						<th>还车门店</th>
						<th>订单状态</th>
						<th>操作</th>
					</tr>
					<c:forEach var="order" items="${orders}">
						<tr>
							<td>${order.id }</td>
							<td>${order.ordertime }</td>
							<td>${order.days }</td>
							<td>${order.user.account }</td>
							<td>${order.car.version }</td>
							<td>${order.takestore.name}</td>
							<td>${order.returnstore.name }</td>
							<td>${order.state }</td>
							<td><a href="orderDetailUI.action?oid=${order.id }&uid=${uid}">查看</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<input  type="hidden" id="_uid" value="${uid }"/>
		</div>
		
		<script type="text/javascript">
				//根据订单状态异步获取订单集合
				function stateChange(){
					var state = $("#_state option:selected").val();
					var uid = $("#_uid").val();
					$.post(
						"ajaxGetOrders.action",
						{
							"state":state,
							"uid":uid
						},
						function(data){
							var table = $("#orderTable");
							table.empty();
							table.append("<tr><th>订单编号</th><th>订单时间</th><th>租用天数</th><th>用户</th><th>车辆型号</th><th>取车门店</th><th>还车门店</th><th>订单状态</th><th>操作</th></tr>");
							$.each(data.orders,function(e,v){
								var tr = $("<tr></tr>");
								var td1 = $("<td>"+v.id+"</td>");
								var td2 = $("<td>"+v.ordertime+"</td>");
								var td3 = $("<td>"+v.days+"</td>");
								var td4 = $("<td>"+v.user.account+"</td>");
								var td5 = $("<td>"+v.car.version+"</td>");
								var td6 = $("<td>"+v.takestore.name+"</td>");
								var td7 = $("<td>"+v.returnstore.name+"</td>");
								var td8 = $("<td>"+v.state+"</td>");
								var td9 = $("<td></td>");
								var a1 = $("<a href='orderDetailUI.action?oid="+v.id+"&uid="+uid+"'>查看</a>");
								td9.append(a1);
								tr.append(td1);
								tr.append(td2);
								tr.append(td3);
								tr.append(td4);
								tr.append(td5);
								tr.append(td6);
								tr.append(td7);
								tr.append(td8);
								tr.append(td9);
								table.append(tr);
							});
						}
					);				
				}
		</script>
	</body>
</html>

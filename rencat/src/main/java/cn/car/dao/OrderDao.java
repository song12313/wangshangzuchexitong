package cn.car.dao;

import java.util.List;

import cn.car.entity.Order;

public interface OrderDao extends BaseDao<Order> {

	/**
	 * 根据用户id查找订单集
	 * 按时间顺序排序
	 * @param id
	 * @return
	 */
	List<Order> findObjectByUserId(String userid);

	/**
	 * 根据订单状态查找订单集
	 * 按订单时间排序
	 * @param state
	 * @return
	 */
	List<Order> findObjectByState(String state);

	void changeOrderState(String sid, String state);

	/**
	 * 根据订单状态和用户id查找订单集
	 * 按订单时间排序
	 * @param state
	 * @return
	 */
	List<Order> findObjectByStateAndUser(String state, String uid);

}

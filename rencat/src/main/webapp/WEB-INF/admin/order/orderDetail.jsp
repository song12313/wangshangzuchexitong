<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@include file="../common.jspf" %>		
		<link rel="stylesheet" type="text/css" href="${basePath }acss/orderDetail.css"/>
	</head>
	<body>
		<div class="_BigBox">
			<a class="_aBtn" href="orderListUI.action?uid=${uid }">返回</a>	
			<div class="_state">
					<select  id="_state"  >
						<option value="wait">待提车</option>
						<option value="renting">租赁中</option>
						<option value="finish">已完成</option>
						<option value="invalid">无效</option>
					</select>
					<a class="_bBtn" href="javascript:dochange()">修改订单状态</a>
			</div>
			<div class="_clearFloat"></div>
			<p class="_title">订单信息</p>
			<div class="_infoBox">
				<div class="_img">
					<img src="${order.car.image }" alt="车辆的照片" />
				</div>
				<div class="_info_l">
					订单编号:<span id="stateid">${order.id }</span>
				</div>
				<div class="_info_l" style="width:260px;">
					下单时间:<span style="color: red;width:180px;">${order.ordertime }</span>
				</div> 
				<div class="_info_l">
					车辆编号:<span>${order.car.id }</span>
				</div>
				<div class="_info_l">
					车辆型号:<span>${order.car.version }</span>
				</div>
				<div class="_info_l">
					取车门店:<span>${order.takestore.name }</span>
				</div>
				<div class="_info_l">
					还车门店:<span>${order.returnstore.name }</span>
				</div>
				<div class="_info_l">
					租赁价格:<span>${order.total }元</span>
				</div>
				<div class="_info_l">
					订单状态:<span style="color: red;" id="orderState">${order.state }</span>
				</div> 
				<div class="_info">
					定金:<span>${order.earnestMoney }</span>
				</div>
				<div class="_info">
					用户:<span>${order.user.account }</span>
				</div>
				<div class="_info_l" style="width: 530px;">
					订单描述:<span style="width: 458px;">${order.description }</span>
				</div> 
			</div>
		</div>
		
		
		<script type="text/javascript">
			function dochange(){
				var state = $("#_state :selected");
				var id = $("#stateid").text();
				var value = state.val();
				$.post(
					"ajaxModifyState.action",
					{"sid":id,
						"state":value}
				);
				$("#orderState").text(value);
				alert("订单状态已改为:"+state.text());
			}
		</script>
	</body>
</html>
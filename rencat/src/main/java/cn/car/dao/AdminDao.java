package cn.car.dao;


import java.util.List;

import cn.car.entity.Admin;

public interface AdminDao extends BaseDao<Admin>{

	/**
	 * 根据帐号和密码查找用户集
	 * @param account
	 * @param password
	 * @return
	 */
	List<Admin> findObjectByAccAndPwd(String account, String password);
}

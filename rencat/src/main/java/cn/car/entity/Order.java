package cn.car.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class Order implements Serializable {

	private String id;
	private int days;						//租期
	private Timestamp ordertime;	//下单时间
	private Timestamp realtime;		//取车时间
	private String description;
	
	private Store takestore;
	private Store returnstore;
	private User user;
	private Car car;
	
	private String state = STATE_WAIT;
	
	public static String STATE_WAIT = "wait";
	public static String STATE_RENTING = "renting";
	public static String STATE_FINISH = "finish";
	public static String STATE_INVALID= "invalid";
	public static Map<String, String> statesMap = new HashMap<String, String>();
	static{
		statesMap.put(STATE_WAIT, "待提车");
		statesMap.put(STATE_RENTING, "租赁中");
		statesMap.put(STATE_FINISH, "已完成");
		statesMap.put(STATE_INVALID, "无效");
	}
	//下面这两个不在数据库里面
	private double earnestMoney;
	private double total;
	
	
	public Order() {
		// TODO Auto-generated constructor stub
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public Timestamp getOrdertime() {
		return ordertime;
	}
	public void setOrdertime(Timestamp ordertime) {
		this.ordertime = ordertime;
	}
	public Timestamp getRealtime() {
		return realtime;
	}
	public void setRealtime(Timestamp realtime) {
		this.realtime = realtime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public double getEarnestMoney() {
		if(earnestMoney==0){
			earnestMoney = car.getEarnestMoney();
		}
		return earnestMoney;
	}
	public double getTotal() {
		if(total==0){
			total = car.getPrice()*days;
		}
		return total;
	}

	public Store getTakestore() {
		return takestore;
	}

	public void setTakestore(Store takestore) {
		this.takestore = takestore;
	}
	public Store getReturnstore() {
		return returnstore;
	}
	public void setReturnstore(Store returnstore) {
		this.returnstore = returnstore;
	}
	
}

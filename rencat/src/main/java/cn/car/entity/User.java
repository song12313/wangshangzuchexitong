package cn.car.entity;

import java.io.Serializable;

public class User implements Serializable{
	
	private String id ;
	private String account;
	private String password;
	private String mobile;
	private String email;
	private String idcard;
	private String name;
	private String sex;
	private String dlicense;
	private String dking;
	private String address;
	private String headimg;
	private boolean state; //默认为false,当为true时为激活状态
	
	
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getDlicense() {
		return dlicense;
	}
	public void setDlicense(String dlicense) {
		this.dlicense = dlicense;
	}
	public String getDking() {
		return dking;
	}
	public void setDking(String dking) {
		this.dking = dking;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getHeadimg() {
		return headimg;
	}
	public void setHeadimg(String headimg) {
		this.headimg = headimg;
	}
	public boolean isState() {
		return state;
	}
	public void setState(boolean state) {
		this.state = state;
	}
	
	
}

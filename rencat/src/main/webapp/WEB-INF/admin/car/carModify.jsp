<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@include file="../common.jspf" %>
		<link rel="stylesheet" type="text/css" href="${basePath }acss/carModify.css"/>
	</head>
	<body>
		<div class="_BigBox">
			<form action="modifyCar.action" method="post" enctype="multipart/form-data">
				<a class="_aBtn" href="javascript:history.go(-1)">返回</a>	
				<div class="_clearFloat"></div>
				<p class="_title">用户资料</p>
				<div class="_infoBox">
					<div class="_img">
						<img src="${car.image }" alt="车辆没有图片" />
					</div>
					<div class="_info_l">
						车辆编号:<input id="" readonly="readonly" name="id" value="${car.id }">
					</div>
					<div class="_info_l">
						发布时间:<input readonly="readonly" name="releaseTime" value="${car.releaseTime }">
					</div> 
					<div class="_info_l">
						车辆型号:<input type="text" name="version" value="${car.version }">
					</div>
					<div class="_info_l">
						租赁价格:<input type="text" name="price" value="${car.price }">
					</div>
					<div class="_info">
						排档:<input type="text" name="gear" value="${car.gear }"/>
					</div>
					<div class="_info">
						定金:<input type="text" name="earnestMoney" value="${car.earnestMoney }"/>
					</div>
					<div class="_info">
						品牌:<input type="text" name="brand" value="${car.brand }"/>
					</div>
					<div class="_info">
						排量:<input type="text" name="displacement" value="${car. displacement}" />
					</div>
					<div class="_info">
						几人座:<input style="width: 80px;" type="text" name="seat" value="${car.seat }"/>
					</div> 
					<!--
					<div class="_info">
						状态:<input style="color: crimson;" value="已租出" readonly="readonly"></input>
					</div> 
					<div class="_info">
						门店:<select name="" style="">
								<option value="123234">香洲店</option>
								<option value="213232">斗门店</option>
								<option value="214422">金湾店</option>
							</select>
					</div>-->
					<div class="_info">
						种类:<select name="type.id" style="">
									<c:forEach var="type" items="${types }">
											<c:choose>
												<c:when test="${type.id ==car.type.id }">
													<option value="${type.id }" selected="selected">${type.name }</option>
												</c:when>
												<c:otherwise>
													<option value="${type.id }">${type.name }</option>
												</c:otherwise>
											</c:choose>
									</c:forEach>
							</select>
					</div>
					<div class="_info_l" style="width: 530px;">
						车辆描述:<input style="width: 453px;" type="text" name="description" value="${car.description }"/>
					</div> 
					
					
					<div class="_info" style="width: 201px;height: 25px;">
						<input style="width: 200px;border: none;" type="file" name="file" id="" />
					</div>
					<input type="hidden" name="image" value="${car.image }"/>
					<input type="button" name="" id="" value="保存修改" class="_bBtn" onclick="doSubmit();"/>
				</div>
			</form>
		</div>
		
		
			<script type="text/javascript">
				function doSubmit(){
					document.forms[0].submit();
				}
			</script>		
	</body>
</html>
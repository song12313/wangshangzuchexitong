package cn.car.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Type implements Serializable {

	private String id;
	private String name;
	
//	private Set<Car> cars = new HashSet<Car>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public Set<Car> getCars() {
//		return cars;
//	}
//	public void setCars(Set<Car> cars) {
//		this.cars = cars;
//	}
	
	
}

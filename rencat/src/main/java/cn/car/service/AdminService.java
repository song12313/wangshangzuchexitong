package cn.car.service;

import cn.car.entity.Admin;

public interface AdminService extends BaseService<Admin> {

	public Admin login(String account,String password);
	
}

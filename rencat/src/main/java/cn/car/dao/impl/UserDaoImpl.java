package cn.car.dao.impl;


import java.util.List;

import org.hibernate.Query;

import cn.car.dao.UserDao;
import cn.car.entity.User;

public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

	public List<User> findObjectByAccAndPwd(String account, String password) {
		Query query = getSession().createQuery("from User where account = ? and password = ?");
		query.setParameter(0, account);
		query.setParameter(1, password);
		return query.list();
	}

}

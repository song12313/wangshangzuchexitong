package cn.car.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cn.car.dao.BaseDao;

public class BaseDaoImpl<T> extends HibernateDaoSupport implements BaseDao<T> {

	private Class<T> clazz;
	public BaseDaoImpl(){
		//通过反射得到T的真实类型
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		clazz = (Class<T>) type.getActualTypeArguments()[0];
	}
	
	public void save(T entity) {
		getHibernateTemplate().save(entity);
	}

	public void delete(Serializable id) {
		getHibernateTemplate().delete(findObjectById(id));
	}

	public void update(T entity) {
		getHibernateTemplate().update(entity);
	}

	public T findObjectById(Serializable id) {
		return (T) getHibernateTemplate().get(clazz, id);
	}

	public List<T> findObjects() {
		Query query = getSession().createQuery("from "+this.clazz.getSimpleName());
		return query.list();
	}

}

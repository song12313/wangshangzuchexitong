package cn.car.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.car.dao.TypeDao;
import cn.car.entity.Type;
import cn.car.service.TypeService;

@Service("typeService")
public class TypeServiceImpl extends BaseServiceImpl<Type> implements
		TypeService {

	private TypeDao typeDao;
	
	@Resource
	public void setTypeDao(TypeDao typeDao) {
		super.setBaseDao(typeDao);
		this.typeDao = typeDao;
	}
}

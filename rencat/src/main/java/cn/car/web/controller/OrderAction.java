package cn.car.web.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.car.entity.Car;
import cn.car.entity.Order;
import cn.car.entity.Store;
import cn.car.entity.User;
import cn.car.service.CarService;
import cn.car.service.OrderService;
import cn.car.service.StoreService;

/**
 * 订单控制器 
 * @author xs
 */
@Controller
public class OrderAction {
	
	@Resource
	private StoreService storeService;
	@Resource
	private OrderService orderService;
	@Resource
	private CarService carService;
	
	@RequestMapping("/orderUI.action")
	public String toOrderUI(String cid,HttpSession session,HttpServletRequest request){
		User userinfo = (User) session.getAttribute("USERINFO");
		if(userinfo!=null){
			if(!userinfo.isState()){
				//帐号未激活,返回个人信息页面
				return "/userInfo.jsp";
			}
			Store takes = (Store) session.getAttribute("takestore");
			Store returns = (Store) session.getAttribute("returnstore");
			if(takes==null || returns ==null){
				return "/rentalCarUI.action";
			}
			Car car = carService.findObjectById(cid);
			request.setAttribute("car", car);
			return "/orderUI.jsp";
		}else{
			request.setAttribute("msg", "请先登录好吗");
			return "/loginUI.jsp";
		}
	}
	
	/**
	 * 异步确定门店,存放在session中
	 * @param tid
	 * @param rid
	 * @param session
	 */
	@RequestMapping("/ajaxEnsureStore.action")
	public void ajaxEnsureStore(String tid,String rid,HttpSession session){
		Store takes = storeService.findObjectById(tid);
		Store returns = storeService.findObjectById(rid);
		session.setAttribute("takestore", takes);
		session.setAttribute("returnstore", returns);
	}
	
	/**
	 * 生成订单
	 * @param tt
	 * @param rt
	 * @param cid
	 * @param session
	 */
	@RequestMapping("/addOrder.action")
	public String addOrder(String time1,String time2,int days,String cid,HttpSession session){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//拿到各类信息
			Store takes = (Store) session.getAttribute("takestore");
			Store returns = (Store) session.getAttribute("returnstore");
			User userinfo = (User) session.getAttribute("USERINFO");
			//直接伪造一个car,这样不用去数据库查找
			Car car = new Car();
			car.setId(cid);
			
			//创建订单实例
			Order order = new Order();
			order.setCar(car);
			order.setDays(days);
			order.setTakestore(takes);
			order.setReturnstore(returns);
			order.setUser(userinfo);
			order.setOrdertime( new Timestamp(new Date().getTime()));
			Date date = sdf.parse(time1+" "+time2+":00");
			order.setRealtime(new Timestamp(date.getTime()));
			order.setState(Order.STATE_WAIT);
			orderService.save(order);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "/userOrderUI.action";
	}
	
	@RequestMapping("/userOrderUI.action")
	public String toUserOrderUI(Model model,HttpSession session){
		User userinfo = (User) session.getAttribute("USERINFO");
		List<Order> orders = orderService.findObjectByUser(userinfo.getId());
		model.addAttribute("orders",orders);
		return "/userOrderUI.jsp";
	}
}

package cn.car.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.car.dao.StoreDao;
import cn.car.entity.Store;
import cn.car.service.StoreService;

@Service("storeService")
public class StoreServiceImpl extends BaseServiceImpl<Store> implements
		StoreService {

	private StoreDao storeDao;
	@Resource
	public void setStoreDao(StoreDao storeDao) {
		super.setBaseDao(storeDao);
		this.storeDao = storeDao;
	}
}

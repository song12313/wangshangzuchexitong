package cn.car.service;

import java.util.List;

import cn.car.entity.Order;

public interface OrderService extends BaseService<Order> {

	/**
	 * 根据用户查找订单集
	 * 按订单时间排序
	 * @param id
	 * @return
	 */
	List<Order> findObjectByUser(String userid);

	/**
	 * 根据订单状态查找订单集
	 * 按订单时间排序
	 * @param state
	 * @return
	 */
	List<Order> findObjectByStateOrUser(String state,String uid);

	/**
	 * 改变订单状态
	 * @param sid
	 * @param state
	 */
	void changeOrderState(String sid, String state);

}

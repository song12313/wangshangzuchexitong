<%@ page language="java"  pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <title>完善个人信息</title>
    <%@include file="common/common.jspf" %>
    <link rel="stylesheet" href="${basePath }css/userInfoUI.css">
</head>
<body>
		<%@include file="common/top.jspf" %>
		<%@include file="common/nav.jspf" %>

<div class="main">
	<form action="modify.action" method="post"  enctype="multipart/form-data">
    <div class="ti">
        <h2>完善个人信息
        	<c:if test="${!USERINFO.state }">
    	  	  <span style="color:red;font-size:14px;">(帐号未激活)</span>
      	 	</c:if>
        </h2>
        <a href="index.jsp">返回</a>
        <a href="userPwdUI.jsp" style="background-color: #F76030;border:1px solid #F76030">修改个人密码</a>
        <a href="userOrderUI.action" style="background-color: #F5B805;border:1px solid #F5B805">查看个人订单</a>
    </div>
    <div class="bo bb">
        <div class="to">
            <div class="xx">
                <span></span><em>用户详细信息</em>
            </div>
            <div class="cr">
                <h2>完善信息</h2>
                <div class="bt">
                    <div class="xm">用户帐号：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="account"  name="account" type="text" value="${USERINFO.account }"  readonly="readonly">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">手机号码：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="mobile" name="mobile" type="text" value="${USERINFO.mobile }"   readonly="readonly">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">邮箱：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="email" name="email" type="text" value="${USERINFO.email }"  readonly="readonly">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">姓名：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="name"  type="text" value="${USERINFO.name }" name="name">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">性别：</div>
                    <div class="wbk">
                        <select name="sex" class="wbk_01" style="width: 100px;" >
                        	<option value="男">男</option>
                        	<option value="女">女</option>
                        </select>
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">身份证号：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="bodycode" type="text" value="${USERINFO.idcard }" placeholder="请输入身份证" name="idcard">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">驾驶证编号：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="bodycode" type="text" value="${USERINFO.dlicense }" placeholder="请输入驾驶证" name="dlicense">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">家庭住址：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="bodycode" type="text" value="${USERINFO.address }" placeholder="请输入家庭住址" name="address">
                    </div>
                </div>
                <div class="bt">
                    <div class="xm">驾驶证类型：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="bodycode" type="text" value="${USERINFO.dking }" placeholder="请输入驾驶证类型" name="dking">
                    </div>
                </div>
                <div class="_headImg">
                	<img src="${USERINFO.headimg }" alt="用户头像" style="width:100%;height:100%;" />
                </div>
                <div class="bt">
                    <div class="xm">头像上传：</div>
                    <div class="wbk">
                        <input class="wbk_01" id="bodycode" type="file" name="file">
                    </div>
                </div>
                <input type="hidden" name="headimg" value="${USERINFO.headimg }" />
            </div>
        </div>

        <div class="xdd">
            <div class="xdd_ri">
                <div class="cc">
                        <input class="cc_01" type="button" id="subtn" value="保存修改" onclick="doSubmit()">
                </div>
            </div>
        </div>
    </div>
    
    	<input name="id"  type="hidden" value="${USERINFO.id }"/>
    </form>
</div>
		
		<%@include file="common/bottom.jspf" %>
		
		
		<script type="text/javascript">
			function doSubmit(){
				document.forms[0].submit();
			}
		</script>
</body>
</html>

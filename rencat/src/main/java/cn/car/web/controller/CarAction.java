package cn.car.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.car.entity.Car;
import cn.car.service.CarService;
import cn.car.web.util.PageBean;

@Controller
public class CarAction {
	
	@Resource
	private CarService carService;
	
	@RequestMapping("/rentalCarUI")
	public String toRentalCarUI(Model model){
		PageBean<Car> page = new PageBean<Car>();
		List<Car> items = carService.findObjects();
		page.setItems(items);
		page.setPageNo(0);
		page.setTotalSize(12);
		model.addAttribute("page",page);
		return "/rentalCarUI.jsp";
	}
	
	@RequestMapping("/carDetailUI")
	public String toCarDetailUI(String cid,Model model){
		Car car = carService.findObjectById(cid);
		model.addAttribute("car",car);
		return "/carDetailUI.jsp";
	}
	
	@RequestMapping("/ajaxGetCar")
	@ResponseBody
	/**
	 * 众多车辆的条件过来,然后分割.
	 * 比如页面过来的condition: type=2,version=宝马v1,price=400
	 * 
	 * @param condition
	 * @return
	 */
	public Map<String, Object> ajaxGetCar(String condition){
		Map<String, String> conditions = new HashMap<String, String>();
		String[] strs = condition.split(",");
		for(String s:strs){
			String[] temp = s.split("=");
			conditions.put(temp[0], temp[1]);
		}
		
		List<Car> cars=carService.findObjectByConditions(conditions);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", "success");
		map.put("cars", cars);
		return map;
	}
}

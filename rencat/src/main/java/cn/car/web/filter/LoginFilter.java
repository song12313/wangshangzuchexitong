package cn.car.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import cn.car.entity.Admin;

import com.sun.net.httpserver.Filter.Chain;

/**
 * 管理员登录过滤器
 * @author xs
 *
 */
public class LoginFilter implements Filter {

	public void init(FilterConfig filterconfig) throws ServletException {

	}

	public void doFilter(ServletRequest requ,
			ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) requ;
		String URL = request.getRequestURI();
		if(URL.contains("/admin/")){
			if(!URL.contains("/login")){
				Admin admin=(Admin) request.getSession().getAttribute("ADMININFO");
				if(admin==null){
					request.setAttribute("msg", "请先登录哦");
					request.getRequestDispatcher("/admin/loginUI.action").forward(request, response);
				}
			}
		}
		chain.doFilter(request, response);
	}

	public void destroy() {

	}

}

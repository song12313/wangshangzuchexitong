<%@ page language="java"  pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <title>车辆预订</title>
    <%@include file="common/common.jspf" %>
	<link rel="stylesheet" type="text/css" href="${basePath }css/userOrderUI.css"/>
</head>
<body>
	<%@include file="common/top.jspf" %>
		<%@include file="common/nav.jspf" %>

<div class="main" style="min-height:500px;">
    <div class="ti">
        <h2>个人订单</h2>
        <a href="javascript:history.go(-1)">返回</a>
    </div>
    <div class="bo bb">
        <div class="to">
            <div class="xx">
                <span></span><em>订单列</em>
            </div>
            
            <style>
				.datatable{width:100%;border: 1px solid gainsboro;overflow: hidden;border-radius: 5px;margin-top: 10px;}
				.datatable tr td{text-align: center;}
				
			</style>
            <div class="cr">
            	<table class="datatable">
            		<tr>
            			<td>订单编号</td><td>订单时间</td><td>取车时间</td><td>租用天数</td>
            			<td>金额</td>
            			<td>车辆型号</td><td>取车门店</td>
            			<td>还车门店</td><td>订单状态</td>
            		</tr>
            		
            		<c:forEach var="order" items="${orders }">
		            		<tr>
		            			<td>${order.id }</td>
		            			<td>${order.ordertime }</td>
		            			<td>${order.realtime }</td>
		            			<td>${order.days }</td>
		            			<td>${order.total }元</td>
		            			<td>${order.car.version }</td>
		            			<td>${order.takestore.name }</td>
		            			<td>${order.returnstore.name }</td>
		            			<td>${order.state!="finish"?"进行中":"已完成" }</td>
		            		</tr>
            		</c:forEach>
            		
            	</table>
            </div>
        </div>
        
         <!--
        	作者：444812313@qq.com
        	时间：2016-12-20
        	描述：项目时间紧急,不做历史订单显示
        	
        <div class="to">
            <div class="xx">
                <span></span><em>历史订单</em>
            </div>
            <div class="cr">
            	
            	<table class="datatable">
            		<tr>
            			<td>订单编号</td><td>订单时间</td><td>取车时间</td><td>租用天数</td>
            			<td>金额</td>
            			<td>车辆型号</td><td>取车门店</td>
            			<td>还车门店</td><td>订单状态</td>
            		</tr>
            		<tr>
            			<td>8a9e1e9f5917a63d015917a6ed890000</td><td>2016-12-19 23:15:32.0</td>
            			<td>2016-12-19 23:15:32.0</td>
            			<td>2</td>
            			<td>234元</td>
            			<td>丰田凯美瑞</td><td>香洲门店</td>
            			<td>金湾门店</td><td>wait</td>
            		</tr>
            	</table>
            </div>
        </div>
        	-->


    </div>
</div>

				<%@include file="common/bottom.jspf" %>
</body>
</html>
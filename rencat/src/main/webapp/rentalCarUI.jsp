<%@ page language="java"  pageEncoding="UTF-8"%>
<!doctype html>
<html>
	<head>
		<meta name="Keywords" content="车辆预订" />
		<meta name="Description" content="车辆预订" />
		<%@include file="common/common.jspf" %>
		<title>车辆预订</title>
		<link rel="stylesheet" href="${basePath }css/rentalCarUI.css" />
		<script type="text/javascript" src="${basePath }js/datepicker/WdatePicker.js"></script>
	</head>

	<body>
		<%@include file="common/top.jspf" %>	
		<%@include file="common/nav.jspf" %>

		<div class="main_box">
			<div class="main_le">
				<div class="main_le_01">

					<div class="qchc">
						<div class="qchc_top">
							<div class="qcmd">取车门店</div>
							<div class="qcmd_01">
								<select class="width-100">
									<option value="1">珠海</option>
								</select>
								<select id="takestore"   class="width-100">
									<option value="" selected="selected">-请选择-</option>
									<option value="m1" >-金湾门店-</option>
									<option value="m2" >-香洲门店-</option>
									<option value="m3" >-斗门门店-</option>
								</select>
							</div>
						</div>
						<div class="qchc_top">
							<div class="qcmd">还车门店</div>
							<div class="qcmd_01">
								<select  class="width-100">
									<option value="1">珠海</option>
								</select>
								<select  id="returnstore" class="width-100">
									<option value="" selected="selected">-请选择-</option>
									<option value="m1" >-金湾门店-</option>
									<option value="m2" >-香洲门店-</option>
									<option value="m3" >-斗门门店-</option>
								</select>
							</div>
						</div>
					</div>
					<input type="hidden" id="takestoreid" />
					<input type="hidden" id="returnstoreid" />
					<div class="qxc" style="cursor:pointer;" onclick="ensureStore()">确定</div>
				</div>
				<div class="xts">【小提示】预定心仪车辆前请先确定以上的取车门店,还车门店哦</div>
				
				<!-- 放条件的盒子 -->
				<div class="jg_box">
					
					<input  type="hidden" id="condition" />
					
					<div class="jg">
						<div class="pp">
							<a href="javascript:;">价格：</a>
						</div>
						<div class="bx selected" id="priceb">
							<a href="javascript:ajaxGetCar('price','b')">不限</a>
						</div>
						<div class="qb" id="priceq">
							<div class='qb_01 ' id="price200">
								<a href="javascript:ajaxGetCar('price','200')">200以下</a>
							</div>
							<div class='qb_01 ' id="price400">
								<a href="javascript:ajaxGetCar('price','400')">200-400</a>
							</div>
							<div class='qb_01 ' id="price600">
								<a href="javascript:ajaxGetCar('price','600')">400-600</a>
							</div>
							<div class='qb_01 ' id="price800">
								<a href="javascript:ajaxGetCar('price','800')">600-800</a>
							</div>
							<div class='qb_01 ' id="price1000">
								<a href="javascript:ajaxGetCar('price','1000')">800-1000</a>
							</div>
						</div>
					</div>
						<div class="jg">
								<div class="pp">
									<a href="javascript:;">类型：</a>
								</div>
								<div class="bx selected" id="typeidb">
									<a href="javascript:ajaxGetCar('typeid','b')">不限</a>
								</div>
								<div class="qb" id="typeidq">
									<div class='qb_01 ' id="typeidt1">
										<a href="javascript:ajaxGetCar('typeid','t1')">商务车</a>
									</div>
									<div class='qb_01 ' id="typeidt2">
										<a href="javascript:ajaxGetCar('typeid','t2')">豪华车</a>
									</div>
									<div class='qb_01 ' id="typeidt3">
										<a href="javascript:ajaxGetCar('typeid','t3')">SUV</a>
									</div>
									<div class='qb_01 ' id="typeidt4">
										<a href="javascript:ajaxGetCar('typeid','t4')">经济车</a>
									</div>
								</div>
						</div>
					
						
					<div class="jg">
						<div class="pp">
							<a href="javascript:;">座位：</a>
						</div>
						<div class="bx selected" id="seatb">
							<a href="javascript:ajaxGetCar('seat','b')">不限</a>
						</div>
						<div class="qb" id="seatq">
							<div class='qb_01 ' id="seat4">
								<a href="javascript:ajaxGetCar('seat','4')">4座</a>
							</div>
							<div class='qb_01 ' id="seat5">
								<a href="javascript:ajaxGetCar('seat','5')">5座</a>
							</div>
							<div class='qb_01 ' id="seat7">
								<a href="javascript:ajaxGetCar('seat','7')">7座</a>
							</div>
						</div>
					</div>
					
				</div>
					<!-- 放条件的盒子结束 -->
					<!-- 放车辆集合的盒子 -->
					<div class="cl_box" id="carBox">
					<c:forEach var="car" items="${page.items }">
					
						<div class="cl_01">
							<div class="czjd_01">
								<div class="czjd">
									<a href="carDetailUI.action?cid=${car.id }"><img src="${car.image }"></a>
								</div>
							</div>
							<div class="czjd_02">
								<h4><a href="#">${car.version }</a></h4>
								<div class="zdd">
									<span>${car.gear }</span>
									<span>${car.seat }座</span>
									<span>${car.type.name }</span>
								</div>
							</div>
							<div class="czjd_03">¥${car.price }元 /日均</div>
							<div class="czjd_04">
								<a href="carDetailUI.action?cid=${car.id }">马上预订</a>
							</div>
						</div>
					</c:forEach>
						
						
					</div>
					<!-- 放车辆集合的盒子结束 -->
					
					<div class="quotes"> 
						<span class="total_page">${page.totalSize } 条记录 ${page.pageNo +1}/1 页</span> 
						<span class="page-start">上一页</span> 
						
						<span class='current page-cur'>1</span>
						<span class="page-start">下一页</span> 
						
						<!--  没时间做分页,所以舍去
						<c:forEach begin="1" end="${page.totalPage }" varStatus="status">
							<c:choose>
								<c:when test="${status.count == page.pageNo+1}">
								</c:when>
								<c:otherwise>
									<a href="#">${status.count }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<a class='page-next' href='#'>下一页</a>
						 -->
						 
					</div>
					
				
			</div>
			
			<div class="main_ri">
				<div class="ggt"><img src='img/fwys_00.jpg' width='300' height='280' alt='随便一张图片而已' /></div>
				<div class="fwyz_h">服务优势</div>
				<div class="fwyz_01">
					<div class="fwyz_t"><img src="img/fwys_01.jpg"></div>
					<div class="fwyz_gd">
						<div class="fwyz_h">价格更低</div>
						<span>无手续费、无服务费、无需押金，透明一口价</span>
					</div>
				</div>
				<div class="fwyz_01">
					<div class="fwyz_t"><img src="img/fwys_02.jpg"></div>
					<div class="fwyz_gd">
						<div class="fwyz_h">租期灵活</div>
						<span>无手续费、无服务费、无需押金，透明一口价</span>
					</div>
				</div>
				<div class="fwyz_01">
					<div class="fwyz_t"><img src="img/fwys_03.jpg"></div>
					<div class="fwyz_gd">
						<div class="fwyz_h">服务更优</div>
						<span>无手续费、无服务费、无需押金，透明一口价</span>
					</div>
				</div>
			</div>
		</div>
		
		<%@include file="common/bottom.jspf" %>
		
		<script type="text/javascript">
			$("#rentalpage").append("<div  class='bdr'></div>");
		
			function ajaxGetCar(type,value){
				$("#"+type+"b").removeClass("selected");	
				$("#"+type+"q div").each(function(i){
					$(this).removeClass("selected");
				});
				$("#"+type+value).addClass("selected");
				var val = $("#condition").val();
				val = val+type+"="+value+",";
				 $("#condition").val(val);
				 
				 $.post(
					"ajaxGetCar.action",
					{"condition":val},
					function(data){
						var carBox = $("#carBox");		
						carBox.empty();
						$.each(data.cars,function(e,v){
							//var cl_01 = $("<div class='cl_01'></div>");
							//var czjd_01 = $("<div class='czjd_01'></div>");
							//var czjd = $("<div class='czjd' ></div>");
							//var a1 = $("<a href='carDetailUI.action?cid="+e.id+"'></a>");
							var div = $("<div class='cl_01'><div class='czjd_01'><div class='czjd'><a href='carDetailUI.action?cid="+v.id+"'><img src='"+v.image+"'></a></div></div><div class='czjd_02'><h4><a href='#'>"+v.version+"</a></h4><div class='zdd'><span>"+v.gear+"</span><span>"+v.seat+"座</span><span>"+v.type.name+"</span></div></div><div class='czjd_03'>¥"+v.price +"元 /日均</div><div class='czjd_04'><a href='carDetailUI.action?cid="+v.id+"'>马上预订</a></div></div>");
							carBox.append(div);
						});
					}
				 );
			}
			
			function ensureStore(){
				var takestoreid = $("#takestore :selected").val();
				var returntoreid = $("#returnstore :selected").val();
				
				if(takestoreid ==''){
					alert("请确定取车门店");
					return;
				}
				
				if(returntoreid ==''){
					alert("请确定还车门店");
					return;
				}
				
				//异步过去后台
				$.post(
					"ajaxEnsureStore.action",
					{"tid":takestoreid,
						"rid":returntoreid}
				);
				alert("确定好了");
			}
			
			//function ajaxGetCarByPageNo(pageNo){}
		</script>
	</body>

</html>